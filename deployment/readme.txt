Example (assumes JRE1.7 is installed)

Linux etc:
  webcrawler.sh 'http://en.wikipedia.org/wiki/Scala_(programming_language)' 2 .*docs.* 1 cached_files

Windows:
  webcrawler.bat http://en.wikipedia.org/wiki/Scala_(programming_language) 2 .*docs.* 1 cached_files

