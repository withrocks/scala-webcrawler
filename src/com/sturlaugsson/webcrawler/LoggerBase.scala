package com.sturlaugsson.webcrawler

trait LoggerBase {
  def log(msg: String, padding: Int = 0, level: Int = 0)

  def debug(msg: String, padding: Int = 0) = {
    log(msg, padding, 0)
  }

  def info(msg: String, padding: Int = 0) = {
    log(msg, padding, 1)
  }
}