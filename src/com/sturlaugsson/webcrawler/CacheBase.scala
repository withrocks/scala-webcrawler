package com.sturlaugsson.webcrawler

trait CacheBase {
  def cache(url: String, page: String): Unit
  def contains(url: String): Boolean
  def get(url: String): String
}