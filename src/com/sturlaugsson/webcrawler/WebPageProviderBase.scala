package com.sturlaugsson.webcrawler

import scala.util.matching.Regex

trait WebPageProviderBase {
  // For simplicity, this returns a String, while in reality a Stream would make more sense
  def fetch(url: String): String
  def getChildPages(page: String, parentUrl: String, includePattern: Regex): List[String]
}