package com.sturlaugsson.webcrawler

import java.net.URL
import org.jsoup.Jsoup
import scala.util.matching.Regex

class WebPageProvider extends WebPageProviderBase {
  def fetch(url: String): String = {
    // TODO: Handle exceptions gracefully
    val page = Jsoup.connect(url).get().html()
    page
  }

  // Returns true if the the url should be included based on the core rules (ignoring user specific patterns)
  private def includeUrl(url: String): Boolean = {
    if (url.startsWith("#")) false
      else true
    }

  private def handleRelativePaths(url: String, parentUrl: String): String = {
    if (url.startsWith("//")) {
      // Should be relative to the protocol
      val supportedProtocols = List("http:", "https:")
      for (protocol <- supportedProtocols) {
         if (parentUrl.startsWith(protocol)) {
           return s"$protocol$url"
         }
      }
      throw new UnsupportedOperationException(s"Protocol in '$parentUrl' was not expected")
    }
    else if (url.startsWith("/")) {
      // Should be relative to the rootUrl
      s"$parentUrl$url"
    }
    else {
      url
    }   
  }

  // Extracts all the links in the page
  private def links(page: String): List[String] = {
    // TODO: could be rewritten to use less imperative style
    val ret = new scala.collection.mutable.MutableList[String]()
    val doc = org.jsoup.Jsoup.parse(page)
    val elements = doc.getElementsByTag("a").listIterator()
    while (elements.hasNext()) {
      val element = elements.next()
      if (element.hasAttr("href")) {
        ret += element.attr("href")
      }
    }
    ret.toList
  }
  
  def getChildPages(page: String, parentUrl: String, includePattern: Regex): List[String] = {
    for 
    {
      link <- links(page) 
      if includeUrl(link)
      if includePattern.pattern.matcher(link).matches
    } yield handleRelativePaths(link, parentUrl)
  }
}