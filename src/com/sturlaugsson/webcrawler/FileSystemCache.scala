package com.sturlaugsson.webcrawler

import java.io.File
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.charset.Charset
import java.nio.file.Paths
import java.io.BufferedReader
import java.io.FileReader

/* Will be treated as a NullCache if the cacheDir is empty, otherwise, caches to that location */
class FileSystemCache(logger: LoggerBase, cacheDir: String = "") extends CacheBase {
  
  if (cacheDir != "") {
    val dirObj = new File(cacheDir);  
    if (!dirObj.exists()) {
      logger.info(s"Creating directory $cacheDir for cached files. Absolute path: " + dirObj.getAbsolutePath())
      dirObj.mkdir()
    } 
  }  
  
  def cache(url: String, page: String): Unit = {
    if (!contains(url)){      
      val fileObj = urlToFile(url)
      val path = fileObj.getAbsolutePath()
      logger.info(s"Caching $url=>$path", 0) 

      val writer = new PrintWriter(path, "UTF-8");
      try {        
        writer.print(page)
      }
      finally {
        writer.close(); 
      }          
    }    
  }

  private def urlToFile(url: String): File = {
    val fileName = cacheKeyFromUrl(url)
    new File(cacheDir, fileName)
  }

  /* Creates a key by replacing punctuation with underscores only. 
   * (Key collision is possible, but ignored for this exercise) */
  def cacheKeyFromUrl(url: String): String = {
    url.map(mapped => if (!mapped.isLetterOrDigit) '_' else mapped)    
  }

  def contains(url: String): Boolean = {
    logger.info(s"Checking if $url exists in cache", 0)
    urlToFile(url).exists()    
  }
  
  def get(url: String): String = {
    logger.info(s"Getting $url from cache", 0)
    val fileObj = urlToFile(url)     

    // TODO: Ugly quick hack. Should be more functional
    val buffer = new BufferedReader(new FileReader(fileObj));
    val fileContents = new StringBuffer()
    var line = buffer.readLine() 
    while (line != null) {
        fileContents.append(line)
        line = buffer.readLine()
    }
    fileContents.toString()
  }
}