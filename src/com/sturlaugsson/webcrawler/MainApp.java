package com.sturlaugsson.webcrawler;

/* Using a Java entry class only so that I can package easily with Eclipse */
public class MainApp
{
  public static void main(String [] args)
  {
    Application app = new Application();
    app.runWithArgs(args);
  }
}