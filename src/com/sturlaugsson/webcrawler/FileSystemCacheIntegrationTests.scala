package com.sturlaugsson.webcrawler

import org.scalatest.FunSuite

/* These are 'manual' integration tests (for simplicity), 
 * cache folder must be manually inspected/deleted between tests */
class FileSystemCacheIntegrationTests extends FunSuite {
  test("Manual: Creating a FileSystemCache object with a non-empty folder name will create a folder") {
    val logger = new Logger(0)
    val cache = new FileSystemCache(logger, "webcrawler_cache_tests")
  }

  test("Manual: Caching a page creates a new file in the cache folder and that file is retrievable from the cache") {
    val logger = new Logger(0)
    val cache = new FileSystemCache(logger, "webcrawler_cache_tests")
    val url = "http://www.google.com"
    val page = "<html></html>"
    assert(!cache.contains(url), "Manual test setup: Remove results from last test")
    cache.cache("http://www.google.com", page)
    assert(cache.contains(url), "Manual test setup: Remove results from last test")
    val otherPage = cache.get(url)
    assert(page === otherPage)
  }
}