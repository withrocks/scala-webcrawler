package com.sturlaugsson.webcrawler

import org.scalatest.FunSuite

class WebPageProviderTests extends FunSuite {
  test("Page with links returns expected links") {
    val provider = new WebPageProvider()
    val links = provider.getChildPages("<html><a href='link1'></a><a>should be ignored</a><a href='link2'></a><html>", "", ".*".r)
    println(links)
    assert(links.length === 2)
    assert(links(0) === "link1")
    assert(links(1) === "link2")
  }
  
  test("Page with no link returns no links") {
    val provider = new WebPageProvider()
    val links = provider.getChildPages("<html><html>", "", ".*".r)
    println(links)
    assert(links.length === 0)
  }
  
  test("Relative paths resolve to non-relative") {
    val provider = new WebPageProvider()
    val relative = "/relative"
    val rootUrl = "http://url"
    val page = s"<a href='$relative'></a>"
    val childPages = provider.getChildPages(page, rootUrl, ".*".r)
    assert(childPages.length === 1)
    assert(childPages(0) === s"$rootUrl$relative")
  }
  
  test("Double slash relative paths should be handled as root urls, using same protocol") {
    val provider = new WebPageProvider()
    val relative = "//relative"
    val protocol = "http:"
    val rootUrl = s"$protocol//url"      
    val page = s"<a href='$relative'></a>"
    val childPages = provider.getChildPages(page, rootUrl, ".*".r)
    assert(childPages.length === 1)
    assert(childPages(0) === s"$protocol$relative")
  }
  
  test("Anchors are ignored") {
    val provider = new WebPageProvider()
    val link = "#someanchor"
    val rootUrl = "http://url"
    val page = s"<a href='$link'></a>"
    val childPages = provider.getChildPages(page, rootUrl, ".*".r)
    assert(childPages.length === 0)
  }
  
  test("Child pages not matching pattern are ignored") {
    val provider = new WebPageProvider()    
    val page = s"<a href='http://link1'></a><a href='http://link2'></a><a href='http://link3'></a>"
    // Only match links ending with link1 or link3:
    val regex = """.*link1$|.*link3$""".r
    
    val childPages = provider.getChildPages(page, "<ignored>", regex)
    assert(childPages.length === 2)
  }
}