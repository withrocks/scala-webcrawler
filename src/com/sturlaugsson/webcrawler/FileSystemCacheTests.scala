package com.sturlaugsson.webcrawler

import org.scalatest.FunSuite

class FileSystemCacheTests extends FunSuite {
  test("Cache key from URL is valid") {
    val logger = new Logger(0)
    // This is a unit test, since the FileSystemCache does not integrate with the file system when the path is empty
    val cache = new FileSystemCache(logger, "") 

    // Cache key must be a valid filename on Linux and Windows
    // One way to achieve that is having all punctuation replaced with underscore
    val key = cache.cacheKeyFromUrl("http://www.google.com")
    assert(key === "http___www_google_com")
  }
}