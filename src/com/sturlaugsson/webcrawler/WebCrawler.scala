package com.sturlaugsson.webcrawler

import scala.util.matching.Regex

class WebCrawler(webPageProvider: WebPageProviderBase, cache: CacheBase, logger: LoggerBase) {
  // Crawls from the base url. Returns the total number of pages crawled
  def crawl(url: String, maxDepth: Int, childPageMatch: Regex): Unit = {
    logger.debug(s"Started crawling '$url'. Will stop at $maxDepth", 0)
    crawlRec(url, maxDepth, 0, childPageMatch)
  }

  // Fetches the page, either from cache or from the provider
  private def getPage(url: String): String = { 
    if (cache.contains(url)) {
      cache.get(url)
    }
    else {
      webPageProvider.fetch(url)
    }
  }

  // Crawls recursively from url.
  private def crawlRec(url: String, maxDepth: Int, currentDepth: Int, childPageMatch: Regex): Unit = {
    // Visit, which is here just getting the page and caching it
    logger.info(s"Visiting '$url'. Depth=$currentDepth. Max=$maxDepth.", currentDepth)
    val page = getPage(url)
    cache.cache(url, page)
  
    if (maxDepth != currentDepth) {
      // TODO: Ensure no cycles
      // NOTE (perf): Whole page in memory
 
      // Now, visit all supbages
      for (url <- webPageProvider.getChildPages(page, url, childPageMatch)) {
        crawlRec(url, maxDepth, currentDepth + 1, childPageMatch)
      }
    }
    else {
      logger.log("Reached maxdepth in subtree.", currentDepth, 0)
    }
  }
}