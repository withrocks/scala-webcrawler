package com.sturlaugsson.webcrawler

import org.scalatest.FunSuite

/* Integrate with the web and the file system */
class WebCrawlerIntegrationTests extends FunSuite {
  test("Fetching wikipedia page on scala results in some items cached") {
    val logger = new Logger(0)
    val cache = new FileSystemCache(logger, "webcrawler_cache_tests")
    val webPageProvider = new WebPageProvider()
    val crawler = new WebCrawler(webPageProvider, cache, logger)
    crawler.crawl("http://en.wikipedia.org/wiki/Scala_(programming_language)", 1, ".*type-dynamic.*".r)
  }
}

class NullCache extends CacheBase {
  def cache(url: String, page: String): Unit = { }
  def contains(url: String): Boolean = { false }
  def get(url: String): String = { "NEVERCALLED" }
}