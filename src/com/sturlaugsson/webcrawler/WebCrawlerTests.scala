package com.sturlaugsson.webcrawler

import scala.collection.mutable.HashMap
import org.scalatest.FunSuite
import scala.util.matching.Regex

// Tests the web crawler by mocking every dependency
class WebCrawlerTests extends FunSuite {
  test("something should happen") {
    // NOTE: Not using a mocking framework here, although I would generally prefer that
    val cache = new CacheMock()
    val webPageProvider = new WebPageProviderMock()
    val logger = new Logger(0)
    webPageProvider.add("http://something", "<html><a href='link'></a></html>")
    val crawler = new WebCrawler(webPageProvider, cache, logger)
    crawler.crawl("http://something", 1, ".*".r)
  }
}

class CacheMock extends CacheBase {
  def cache(url: String, page: String): Unit = {    
  }
  
  def contains(url: String): Boolean = {
    false
  }
  
  def get(url: String): String = {
    ""
  }
}

class WebPageProviderMock() extends WebPageProviderBase {
  val pageMap: HashMap[String, String] = new HashMap[String, String]
  val childPageMap: HashMap[String, List[String]] = new HashMap[String, List[String]]
  
  def add(url: String, page: String) = {
    pageMap += (url -> page)
  }
  
  def addChildren(url: String, children: List[String]) {
    childPageMap += (url -> children)
  }
  
  def fetch(url: String): String = {
    pageMap(url)
  }
  
  def getChildPages(page: String, parentUrl: String, childPagePattern: Regex): List[String] = { 
    List() 
  }
}