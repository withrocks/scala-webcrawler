package com.sturlaugsson.webcrawler

import scala.util.matching.Regex

class Application {
  def runWithArgs(args: Array[String]): Unit = {
    val startUrl = if (args.length > 0) args(0) else ""
    val maxDepth = if (args.length > 1) args(1).toInt else 0
    val includePattern = if (args.length > 2) args(2).r else s"$startUrl.*".r
    val logLevel = if (args.length > 3) args(3).toInt else 1
    val cacheFolder = if (args.length > 4) args(4) else ""

    if (startUrl == "" || maxDepth > 5) {
      printHelp()
    }    
    else {
      run(startUrl, maxDepth, includePattern, logLevel, cacheFolder)
    }
  }
  
  def printHelp(): Unit = {
    println("""|Usage: WebCrawlerApp [rootUrl] <maxDepth> <includePattern>
               |  rootUrl:        The root URL to start scanning from
               |  maxDepth:       The max depth [0,5]
               |  includePattern: Pattern (regex) of which child pages are in scope
                                  By default these are all pages that start with the rootUrl
               |  logLevel:       0=Debug, 1=Info
               |  cacheFolder:    Folder to cache pages to. If not supplied cache will not be used.""".stripMargin)     
  }  
  
  def run(startUrl: String, maxDepth: Int, includePattern: Regex, logLevel: Int, cacheFolder: String): Unit = {
    println("=== Settings ===")
    println(s"Starting from $startUrl")
    println(s"Stopping at depth $maxDepth")
    println(s"Including child pages matching $includePattern")
    println(s"Cache: '$cacheFolder'")
    println("================")
    
    // Set up crawler and dependencies
    val logger = new Logger(logLevel)
    val cache = new FileSystemCache(logger, cacheFolder)
    val webPageProvider = new WebPageProvider()    
    val crawler = new WebCrawler(webPageProvider, cache, logger)
    
    // Do work
    crawler.crawl(startUrl, maxDepth, includePattern)
  }
}