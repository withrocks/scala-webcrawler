package com.sturlaugsson.webcrawler

class Logger(logLevel: Int) extends LoggerBase {
  def log(msg: String, padding: Int = 0, level: Int = 0) = {
    if (level >= logLevel) {
      println((" " * padding) + msg)
    }
  }
}