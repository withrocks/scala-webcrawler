# README #

* This is a super simple webcrawler, written in Scala. It's only intended as an exercise in Scala.

### Setup and test ###

* Download webcrawler_0_0_1.zip and unzip
* chmod +x webcrawler.sh
* Run e.g.:
```
#!bash

# This will fetch articles in this wikipedia page, and then visit all child pages that have the string "docs" in it 
# Limited to depth=2
# Pages will be cached to the folder ./cached_files
./webcrawler.sh 'http://en.wikipedia.org/wiki/Scala_(programming_language)' 2 .*docs.* 1 cached_files
```


* Check out unit and integration tests in the src.



NOTE: The crawler could easily download a bunch of files if the wildcard isn't restrictive - as is the nature of web crawlers.